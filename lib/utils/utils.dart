import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:url_launcher/url_launcher.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

void urlLauncher(String url) async {
  if (!await launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication)) {
    throw Exception('Could not open $url');
  }
}

void centerToast(BuildContext context, String description, bool isSuccess) {
  showToast(
    description,
    context: context,
    position: StyledToastPosition.center,
    animation: StyledToastAnimation.none,
    reverseAnimation: StyledToastAnimation.fade,
    textPadding: EdgeInsets.symmetric(horizontal: 18, vertical: 8),
    backgroundColor: isSuccess ? HexColor("#6AC259") : HexColor("#E02020"),
    textStyle: Theme.of(context).textTheme.titleMedium?.apply(color: Colors.white),
    duration: Duration(milliseconds: 1500),
  );
}

extension StringMiddaleware on String {
  bool isEmailValid() {
    final expression =
        "^[a-zA-Z0-9.!#\$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*\$";
    return RegExp(expression, caseSensitive: true).hasMatch(this);
  }
}
