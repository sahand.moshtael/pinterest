import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pinterest/login/bloc/login_bloc.dart';

class Apis {
  static const String apiUrl = 'http://192.168.3.73:8001';

  static Future<void> login(String email, String password, Function(String message, LoginStatus status) callback) async {
    try {
      final response = await http.post(
        Uri.parse('$apiUrl/account/api/login/'),
        body: json.encode({"email": email, "password": password}),
        headers: {"Content-Type": "application/json"},
      ).timeout(const Duration(seconds: 10));
      if (response.statusCode == 200) {
        callback(json.decode(response.body)['message'], LoginStatus.SUCCESS);
      } else {
        callback('Something wrong', LoginStatus.FAILURE);
      }
    } catch (e) {
      print(e);
      callback('Please check your internet', LoginStatus.SUCCESS);
    }
  }
}
