import 'package:flutter/material.dart';
import 'package:pinterest/utils/utils.dart';

class AccountSr extends StatefulWidget {
  const AccountSr({super.key});

  @override
  State<AccountSr> createState() => _AccountSrState();
}

class _AccountSrState extends State<AccountSr> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 16.0),
              IconTheme(
                data: IconThemeData(color: Colors.black),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Icon(Icons.share),
                    const SizedBox(width: 24.0),
                    Icon(Icons.settings),
                    const SizedBox(width: 16.0),
                  ],
                ),
              ),
              const SizedBox(height: 24.0),
              CircleAvatar(
                minRadius: 62.0,
                backgroundColor: HexColor('#F1F1F1'),
                child: Text('S', style: theme.textTheme.headlineLarge?.copyWith(fontWeight: FontWeight.w700)),
              ),
              const SizedBox(height: 16.0),
              Text('Sahand Moshtael', style: theme.textTheme.headlineLarge?.copyWith(fontWeight: FontWeight.w700)),
              Text('@sahandmoshtael', style: theme.textTheme.bodyMedium?.copyWith(color: theme.disabledColor, fontWeight: FontWeight.w600)),
            ],
          ),
        ),
      ),
    );
  }
}
