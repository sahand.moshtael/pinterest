import 'package:equatable/equatable.dart';
import 'package:pinterest/global/apis.dart';
import 'package:pinterest/utils/utils.dart';
import 'package:pinterest/widgets/input/export.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(const LoginState()) {
    on<LoginEv>(_login);
  }
}

void _login(LoginEv event, Emitter<LoginState> emit) async {
  if (event.email.isEmailValid()) {
    emit(LoginState(status: LoginStatus.LOADING));
    await Apis.login(
      event.email,
      event.password,
      (message, status) {
        //Callback Http Response
        emit(LoginState(status: status, message: message));
      },
    );
  } else if (event.email.isEmpty) {
    emit(LoginState(status: LoginStatus.FAILURE, message: 'Email is required'));
  } else {
    emit(LoginState(status: LoginStatus.FAILURE, message: 'Email is not valid'));
  }
  emit(LoginState(status: LoginStatus.IN_FORM));
}
