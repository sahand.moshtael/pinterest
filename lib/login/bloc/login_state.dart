part of 'login_bloc.dart';

class LoginState extends Equatable {
  final LoginStatus status;
  final String message;

  const LoginState({this.status = LoginStatus.IN_FORM, this.message = ''});

  @override
  List<Object> get props => [status];
}

enum LoginStatus {
  IN_FORM,
  LOADING,
  FAILURE,
  SUCCESS;
}
