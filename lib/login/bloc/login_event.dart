part of 'login_bloc.dart';

sealed class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class LoginEv extends LoginEvent {
  final String email;
  final String password;

  const LoginEv({required this.email, required this.password});
}
