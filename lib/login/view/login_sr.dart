import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:pinterest/dashboard/view/dashboard_sr.dart';
import 'package:pinterest/global/loading.dart';
import 'package:pinterest/home/view/home_sr.dart';
import 'package:pinterest/login/bloc/login_bloc.dart';
import 'package:pinterest/utils/utils.dart';
import 'package:pinterest/widgets/button.dart';
import 'package:pinterest/widgets/input/export.dart';
import 'package:pinterest/widgets/svg_pic.dart';

class LoginSr extends StatefulWidget {
  const LoginSr({super.key});

  @override
  State<LoginSr> createState() => _LoginSrState();
}

class _LoginSrState extends State<LoginSr> {
  late LoginBloc _loginBloc;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    _loginBloc = context.read<LoginBloc>();
    final styleTapTerms = theme.textTheme.bodySmall?.apply(color: HexColor('#0074E8'));
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        switch (state.status) {
          case LoginStatus.LOADING:
            Loading(context);
            break;
          case LoginStatus.SUCCESS:
            Navigator.pop(context);
            centerToast(context, state.message, true);
            Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardSr()));
            break;
          case LoginStatus.FAILURE:
            centerToast(context, state.message, false);
            break;
          default:
            print('Unkown');
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 4,
                  child: const SizedBox(),
                ),
                Expanded(
                  flex: 6,
                  child: Column(
                    children: [
                      Expanded(
                        child: ListView(
                          physics: const ClampingScrollPhysics(),
                          padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.1),
                          children: [
                            SvgPic('ic_logo', width: 82.0, height: 82.0),
                            const SizedBox(height: 8.0),
                            Text(
                              'Welcome to Pinterest',
                              textAlign: TextAlign.center,
                              style: theme.textTheme.headlineMedium?.apply(color: HexColor('#404040')),
                            ),
                            const SizedBox(height: 24.0),
                            InputField(hint: 'Email address'),
                            const SizedBox(height: 32.0),
                            Button(
                              text: 'Continue',
                              textColor: Colors.white,
                              backgroundColor: HexColor('#E60023'),
                              onPress: () => _loginBloc.add(LoginEv(
                                email: context.read<InputBloc>().state.controller!.text,
                                password: '',
                              )),
                            ),
                            const SizedBox(height: 24.0),
                            Button(
                              text: 'Continue with Facebook',
                              icon: Icon(Icons.facebook, color: Colors.white),
                              textColor: Colors.white,
                              backgroundColor: HexColor('#1877F2'),
                              onPress: () {},
                            ),
                            const SizedBox(height: 8.0),
                            Button(
                              text: 'Continue with Google',
                              icon: SvgPic('ic_google', width: 24.0),
                              textColor: Colors.black,
                              backgroundColor: HexColor('#E9E9E9'),
                              onPress: () {},
                            ),
                            const SizedBox(height: 16.0),
                            Text.rich(
                              TextSpan(
                                style: theme.textTheme.bodySmall,
                                children: [
                                  const TextSpan(text: "By continuing, you agree to Pinterest's"),
                                  const TextSpan(text: " "),
                                  TextSpan(
                                    text: "Terms of Service",
                                    style: styleTapTerms,
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () => urlLauncher('https://policy.pinterest.com/en/terms-of-service-2016'),
                                  ),
                                  const TextSpan(text: " "),
                                  const TextSpan(text: "and acknowledge that you've read our"),
                                  const TextSpan(text: " "),
                                  TextSpan(
                                    text: "Privacy Policy. Notice at collection",
                                    style: styleTapTerms,
                                    recognizer: TapGestureRecognizer()..onTap = () => urlLauncher('https://policy.pinterest.com/en/privacy-policy'),
                                  ),
                                ],
                              ),
                              textAlign: TextAlign.center,
                            ),
                            const SizedBox(height: 8.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
