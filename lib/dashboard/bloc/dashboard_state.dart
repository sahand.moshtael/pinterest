part of 'dashboard_bloc.dart';

class DashboardState extends Equatable {
  final Map<int, Widget> pages = {
    0: HomeSr(),
    4: AccountSr(),
  };
  final int currentIndex;
  DashboardState({this.currentIndex = 0});

  @override
  List<Object> get props => [currentIndex];
}
