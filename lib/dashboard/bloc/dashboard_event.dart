part of 'dashboard_bloc.dart';

sealed class DashboardEvent extends Equatable {
  const DashboardEvent();

  @override
  List<Object> get props => [];
}

class ChangeBtmItem extends DashboardEvent {
  final int index;

  const ChangeBtmItem(this.index);
}
