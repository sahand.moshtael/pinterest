import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:pinterest/account/view/account_sr.dart';
import 'package:pinterest/home/view/home_sr.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  DashboardBloc() : super(DashboardState()) {
    on<ChangeBtmItem>(_btmNavOnTap);
  }
}

void _btmNavOnTap(ChangeBtmItem event, Emitter<DashboardState> emit) {
  emit(DashboardState(currentIndex: event.index));
}
