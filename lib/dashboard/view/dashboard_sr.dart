import 'package:flutter/material.dart';
import 'package:pinterest/dashboard/bloc/dashboard_bloc.dart';
import 'package:pinterest/widgets/btm_nav_bar.dart';
import 'package:pinterest/widgets/input/export.dart';

class DashboardSr extends StatefulWidget {
  const DashboardSr();

  @override
  State<DashboardSr> createState() => _DashboardSrState();
}

class _DashboardSrState extends State<DashboardSr> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DashboardBloc, DashboardState>(
      builder: (context, state) => WillPopScope(
        onWillPop: () async {
          if (state.currentIndex == 0) {
            return true;
          }
          context.read<DashboardBloc>().add(ChangeBtmItem(0));
          return false;
        },
        child: Scaffold(
          body: state.pages[state.currentIndex],
          bottomNavigationBar: BtmNavBar(
            currentIndex: state.currentIndex,
            onTap: (index) => context.read<DashboardBloc>().add(ChangeBtmItem(index)),
          ),
        ),
      ),
    );
  }
}
