import 'package:flutter/material.dart';
import 'package:pinterest/widgets/svg_pic.dart';

class HomeSr extends StatefulWidget {
  const HomeSr({super.key});

  @override
  State<HomeSr> createState() => _HomeSrState();
}

class _HomeSrState extends State<HomeSr> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GridView.count(
          physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
          crossAxisCount: 2,
          crossAxisSpacing: 16.0,
          padding: const EdgeInsets.only(bottom: 72.0, left: 16.0, right: 16.0),
          children: List.generate(15, (index) {
            return Column(
              children: [
                Card(
                  clipBehavior: Clip.hardEdge,
                  child: Container(
                    color: Colors.black,
                    height: 100.0,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('Amoung'),
                        Text('wallpapers, free'),
                      ],
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Icon(
                        Icons.more_horiz,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ],
            );
          }),
        ),
      ),
    );
  }
}
