import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pinterest/account/view/account_sr.dart';
import 'package:pinterest/dashboard/bloc/dashboard_bloc.dart';
import 'package:pinterest/dashboard/view/dashboard_sr.dart';
import 'package:pinterest/home/view/home_sr.dart';
import 'package:pinterest/login/bloc/login_bloc.dart';
import 'package:pinterest/login/view/login_sr.dart';
import 'package:pinterest/utils/utils.dart';
import 'package:pinterest/widgets/input/bloc/input_bloc.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.white,
    statusBarIconBrightness: Brightness.dark,
  ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => InputBloc()),
        BlocProvider(create: (context) => LoginBloc()),
        BlocProvider(create: (context) => DashboardBloc()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: HexColor('#E60023')),
          useMaterial3: true,
        ),
        debugShowCheckedModeBanner: false,
        home: LoginSr(),
      ),
    );
  }
}
