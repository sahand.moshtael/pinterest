import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinterest/widgets/box.dart';

class Button extends StatelessWidget {
  final Color backgroundColor;
  Widget? icon;
  final String text;
  final Color textColor;
  final VoidCallback onPress;

  Button({
    required this.backgroundColor,
    required this.text,
    required this.textColor,
    required this.onPress,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      minSize: 0.0,
      padding: EdgeInsets.zero,
      onPressed: onPress,
      child: Box(
        child: Row(
          children: [
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: icon ?? const SizedBox(),
              ),
            ),
            Text(
              text,
              style: Theme.of(context).textTheme.titleMedium?.apply(color: textColor),
            ),
            Expanded(child: const SizedBox()),
          ],
        ),
        color: backgroundColor,
      ),
    );
  }
}
