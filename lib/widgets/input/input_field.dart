import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pinterest/utils/utils.dart';
import 'package:pinterest/widgets/box.dart';
import 'package:pinterest/widgets/input/bloc/input_bloc.dart';

class InputField extends StatefulWidget {
  final String? hint;
  const InputField({this.hint});

  @override
  State<InputField> createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> {
  late InputBloc _inputBloc;

  @override
  Widget build(BuildContext context) {
    _inputBloc = context.read<InputBloc>();
    final theme = Theme.of(context);
    return BlocBuilder<InputBloc, InputState>(
      builder: (context, state) {
        return Box(
          child: TextField(
            controller: _inputBloc.state.controller,
            style: theme.textTheme.titleMedium?.copyWith(height: 1.6),
            keyboardType: TextInputType.emailAddress,
            textInputAction: TextInputAction.done,
            onChanged: (value) => _inputBloc.add(ChangeInputEvent(text: value)),
            decoration: InputDecoration(
              hintText: widget.hint,
              hintStyle: theme.textTheme.titleMedium?.apply(color: theme.hintColor),
              border: InputBorder.none,
              isDense: true,
              suffixIcon: state.closeVisible
                  ? GestureDetector(
                      onTap: () => _inputBloc.add(ClearInputEvent(controller: _inputBloc.state.controller!)),
                      child: Icon(Icons.cancel_rounded, size: 24.0),
                    )
                  : null,
            ),
          ),
          color: HexColor('#F1F1F1'),
        );
      },
    );
  }
}
