part of 'input_bloc.dart';

class InputState extends Equatable {
  final bool closeVisible;
  TextEditingController? controller;
  FocusNode? focusNode;

  InputState({this.closeVisible = false, this.controller}) {
    if (controller == null) {
      controller = TextEditingController();
    }
    if (focusNode == null) {
      focusNode = FocusNode();
    }
  }

  @override
  List<Object?> get props => [closeVisible];
}
