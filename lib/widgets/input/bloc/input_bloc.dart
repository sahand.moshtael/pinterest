import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'input_event.dart';
part 'input_state.dart';

class InputBloc extends Bloc<InputEvent, InputState> {
  InputBloc() : super(InputState()) {
    on<ChangeInputEvent>(_onChangeInput);
    on<ClearInputEvent>(_onClearInput);
  }
}

void _onChangeInput(ChangeInputEvent event, Emitter<InputState> emit) {
  emit(InputState(closeVisible: event.text.isNotEmpty));
}

void _onClearInput(ClearInputEvent event, Emitter<InputState> emit) {
  event.controller.clear();
  emit(InputState(closeVisible: false));
}
