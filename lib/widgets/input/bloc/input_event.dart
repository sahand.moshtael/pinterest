part of 'input_bloc.dart';

@immutable
sealed class InputEvent extends Equatable {
  const InputEvent();

  @override
  List<Object?> get props => [];
}

class ChangeInputEvent extends InputEvent {
  final String text;

  const ChangeInputEvent({required this.text});

  @override
  List<Object?> get props => [text];
}

class ClearInputEvent extends InputEvent {
  final TextEditingController controller;

  const ClearInputEvent({required this.controller});

  @override
  List<Object?> get props => [controller];
}
