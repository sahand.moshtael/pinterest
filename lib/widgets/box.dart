import 'package:flutter/material.dart';

class Box extends StatelessWidget {
  final Widget child;
  final Color color;

  const Box({
    required this.child,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 46.0,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: color,
      ),
      child: child,
    );
  }
}
