import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SvgPic extends StatelessWidget {
  final String name;
  double? width;
  double? height;

  SvgPic(
    this.name, {
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    final validName = name.toLowerCase().endsWith('.svg') ? name : '$name.svg';
    return SvgPicture.asset(
      'assets/$validName',
      width: width,
      height: height,
    );
  }
}
